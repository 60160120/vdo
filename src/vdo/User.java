/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vdo;

/**
 *
 * @author informatics
 */
public class User {
    int id;
    String login;
    String name;
    String surname;
    String password;
    String tell;
    int age;
    float weigth;
    float height;

    public User(){
        
    }
    public User(int id, String login, String name, String surname, String password, String tell, int age, float weigth, float height) {
        this.id = id;
        this.login = login;
        this.name = name;
        this.surname = surname;
        this.password = password;
        this.tell = tell;
        this.age = age;
        this.weigth = weigth;
        this.height = height;
    }

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPassword() {
        return password;
    }

    public String getTell() {
        return tell;
    }

    public int getAge() {
        return age;
    }

    public float getWeigth() {
        return weigth;
    }

    public float getHeight() {
        return height;
    }
}
